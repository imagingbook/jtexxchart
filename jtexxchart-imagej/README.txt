Maven notes:

https://maven.apache.org/plugins/maven-shade-plugin/
http://www.avajava.com/tutorials/lessons/how-do-i-build-a-jar-file-that-contains-its-dependencies.html?page=1

see here for parameters: https://maven.apache.org/plugins/maven-shade-plugin/shade-mojo.html

To use the shade plugin do 
Run as "clean package"

use this: <outputFile>ImageJ/jars/${project.artifactId}.jar</outputFile>

alternatively use (makes other jar files!):
<finalName>uber-${project.artifactId}-${project.version}</finalName>
<outputDirectory>ImageJ/jars</outputDirectory>


<exclude>com.imagingbook:jtexxchart-imagej</exclude>