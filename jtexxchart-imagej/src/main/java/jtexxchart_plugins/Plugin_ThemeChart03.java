package jtexxchart_plugins;

import ij.IJ;
import ij.plugin.PlugIn;
import imagingbook.jtexxchart.imagej.IjSwingWrapper;
import imagingbook.lib.ij.IjLogStream;
import imagingbook.other.xchart.Chart;
import imagingbook.other.xchart.demo.charts.theme.ThemeChart03;

/**
 * WB: Matlab Theme
 * @author WB
 *
 */
public class Plugin_ThemeChart03 implements PlugIn {
	
	static {
		IjLogStream.redirectSystem();
	}
	
	@Override
	public void run(String arg0) {
		IJ.log("starting");	// THERE MUST BE SOME OUTPUT, otherwise plugin stalls on launch!!! - problem with output stream!!!
		Chart chart =  new ThemeChart03().getChart();
		new IjSwingWrapper(chart).displayChart();
	}
}
