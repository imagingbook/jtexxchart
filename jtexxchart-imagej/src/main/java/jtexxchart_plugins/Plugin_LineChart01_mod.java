package jtexxchart_plugins;

import java.util.ArrayList;
import java.util.List;

import ij.IJ;
import ij.plugin.PlugIn;
import imagingbook.jtexxchart.examples.LineChart02;
import imagingbook.jtexxchart.imagej.IjSwingWrapper;
import imagingbook.lib.ij.IjLogStream;
import imagingbook.other.xchart.Chart;
import imagingbook.other.xchart.ChartBuilder;
import imagingbook.other.xchart.StyleManager.ChartType;
import imagingbook.other.xchart.StyleManager.LegendPosition;


public class Plugin_LineChart01_mod implements PlugIn {

	static {
		IjLogStream.redirectSystem();
	}

	@Override
	public void run(String arg0) {
		IJ.log("starting");	// THERE MUST BE SOME OUTPUT, otherwise plugin stalls on launch!!! - problem with output stream!!!
		Chart chart = getChart();
		new IjSwingWrapper(chart).displayChart();
	}


	public Chart getChart() {

		// generates Log data
		List<Integer> xData = new ArrayList<Integer>();
		List<Double> yData = new ArrayList<Double>();
		for (int i = -3; i <= 3; i++) {
			xData.add(i);
			yData.add(Math.pow(10, i));
		}

		// Create Chart
		//Chart chart = new ChartBuilder().width(800).height(600).build();
		//Chart chart = new ChartBuilder().chartType(ChartType.Bar).width(800).height(600).title("Score Histogram").xAxisTitle("Score").yAxisTitle("Number").build();
		Chart chart = new ChartBuilder().chartType(ChartType.Bar).width(800).height(600).build();


		// Customize Chart
		chart.getStyleManager().setChartTitleVisible(false);
		chart.getStyleManager().setLegendPosition(LegendPosition.InsideNW);
		//chart.getStyleManager().setYAxisLogarithmic(true);
		// chart.getStyleManager().setXAxisLabelAlignment(TextAlignment.Right);
		chart.getStyleManager().setXAxisLabelRotation(45);
		// chart.getStyleManager().setXAxisLabelRotation(90);
		// chart.getStyleManager().setXAxisLabelRotation(0);
		
		chart.getStyleManager().setBarWidthPercentage(2);
		chart.getStyleManager().setBarFilled(true);

		// Series
		chart.addSeries("$10^x$", xData, yData);

		return chart;
	}

}
