package jtexxchart_plugins;

import ij.IJ;
import ij.plugin.PlugIn;
import imagingbook.jtexxchart.examples.LineChart02;
import imagingbook.jtexxchart.imagej.IjSwingWrapper;
import imagingbook.lib.ij.IjLogStream;
import imagingbook.other.xchart.Chart;


public class Plugin_LineChart02 implements PlugIn {
	
	static {
		IjLogStream.redirectSystem();
	}
	
	@Override
	public void run(String arg0) {
		IJ.log("starting");	// THERE MUST BE SOME OUTPUT, otherwise plugin stalls on launch!!! - problem with output stream!!!
		Chart chart =  new LineChart02().getChart();
		new IjSwingWrapper(chart).displayChart();
	}

}
