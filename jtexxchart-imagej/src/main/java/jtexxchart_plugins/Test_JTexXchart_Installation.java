package jtexxchart_plugins;

import ij.IJ;
import ij.plugin.PlugIn;
import imagingbook.lib.ij.IjLogStream;



public class Test_JTexXchart_Installation implements PlugIn {
	
//	static {
//		IjLogStream.redirectSystem();
//	}
	
	
	@Override
	public void run(String arg0) {
		IJ.log("Testing JTexXchart installation:");
		boolean success = 
			checkClass("imagingbook.jtexxchart.Settings") &
			checkClass("imagingbook.jtexxchart.fonts.Fonts") &
			checkClass("org.scilab.forge.jlatexmath.Box") &
			checkClass("imagingbook.other.xchart.Chart") &
			checkClass("imagingbook.other.xchart.demo.ChartInfo") &
			
			checkResource("org.scilab.forge.jlatexmath.TeXFormula", "DefaultTeXFont.xml");
		
		IJ.log("Summary: " + (success ? "OK" : "ERROR"));
	}
	
	
	
	// the 2 methods below are now defined in imagingbook.lib.util.FileUtils.java
	
	/**
	 * Checks 'by name' if a particular class exists.
	 * 
	 * @param classname fully qualified name of the class, e.g. {@literal imagingbook.lib.util.FileUtils}
	 * @return {@code true} if the class was found, {@code false} otherwise
	 */
	public static boolean checkClass(String classname) {
		String logStr = "  checking class " + classname + " ... ";
		try {
			if (Class.forName(classname) != null) {
				IJ.log(logStr + "found");
				return true;
			}
		} catch (ClassNotFoundException e) {
		}
		IJ.log(logStr + "NOT FOUND");
		return false;
	}
	

	/**
	 * Checks 'by name' of a particular resource exists.
	 * 
	 * @param classname name of the class, e.g. {@literal imagingbook.lib.util.FileUtils}
	 * @param recourcePath path (relative to the location of the class) to the specified resource 
	 * @return
	 */
	public static boolean checkResource(String classname, String recourcePath) {
		String logStr = "  checking resource " + classname + ":" + recourcePath + " ... ";
		try {
			//if (Class.forName(classname).getResourceAsStream(recourcePath) != null) {
			if (Class.forName(classname).getResource(recourcePath)!= null) {
				IJ.log(logStr + "found");
				return true;
			}
		} catch (Exception e) {
			
		}
		IJ.log(logStr + "NOT FOUND");
		return false;
	}

}
