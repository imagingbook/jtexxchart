package imagingbook.jtexxchart.imagej;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.WindowConstants;

import ij.WindowManager;
import imagingbook.other.xchart.Chart;
import imagingbook.other.xchart.SwingWrapper;
import imagingbook.other.xchart.XChartPanel;

/**
 * This is an extension to the {@code SwingWrapper} class to be used
 * in ImageJ as a substitute.
 * 
 * @author WB
 *
 */
public class IjSwingWrapper extends SwingWrapper {

	public IjSwingWrapper(Chart chart) {
		super(chart);
	}
	
	public IjSwingWrapper(List<Chart> charts) {
		super(charts);
	}
	
	public IjSwingWrapper(List<Chart> charts, int numRows, int numColumns) {
		super(charts, numRows, numColumns);
	}
	
	// ----------------------------------------------------
	
	/**
	 * Display the chart in a Swing JFrame
	 */
	@Override
	public JFrame displayChart() {
		// Create and set up the window.
		final JFrame frame = new JFrame(windowTitle);
		// Schedule a job for the event-dispatching thread:
		// creating and showing this application's GUI.
		javax.swing.SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				frame.setDefaultCloseOperation(WindowConstants.HIDE_ON_CLOSE); // wilbur
				JPanel chartPanel = new XChartPanel(charts.get(0));
				frame.add(chartPanel);

				// Display the window.
				frame.pack();
				frame.setVisible(true);
			}
		});
		
		// http://stackoverflow.com/questions/9093448/do-something-when-the-close-button-is-clicked-on-a-jframe
		frame.addWindowListener(new WindowAdapter() {
		    @Override
		    public void windowClosing(WindowEvent windowEvent) {
		        if (JOptionPane.showConfirmDialog(frame, 
		            "Are you sure to close this window?", "Really Closing?", 
		            JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE) 
		        		== JOptionPane.YES_OPTION) {
		        	System.out.println("closing frame now");
		            frame.setVisible(false);
		            frame.dispose();
		            WindowManager.removeWindow(frame);
		            System.out.println("frame closed.");
		        }
		    }
		});
		
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException
				| UnsupportedLookAndFeelException e) { }
				
		WindowManager.addWindow(frame);
		return frame;
	}
}
