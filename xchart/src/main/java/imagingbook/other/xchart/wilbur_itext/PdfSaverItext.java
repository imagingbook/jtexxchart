package imagingbook.other.xchart.wilbur_itext;

import java.awt.Graphics2D;
import java.awt.color.ColorSpace;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import org.scilab.forge.jlatexmath.ext.MathRendering;

import imagingbook.other.xchart.Chart;

import com.itextpdf.awt.PdfGraphics2D;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.ICC_Profile;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfWriter;

/**
 * Concrete implementation of {@link PdfSaver} using the iText PDF library.
 * @author wilbur
 *
 */
public class PdfSaverItext extends PdfSaver {
	
	public PdfSaverItext() {
		super(true);
	}
			
	public PdfSaverItext(boolean renderTextOutline) {
		super(renderTextOutline);
	}

	public String save(Chart chart, String path) throws FileNotFoundException {
		final int width = chart.getWidth();
		final int height = chart.getHeight();
		
		Document document = new Document(new Rectangle(width, height));
		
		File f = new File(path);
		String absPath = f.getAbsolutePath();
		FileOutputStream fos = new FileOutputStream(f);
		
		PdfWriter writer = null;
		
		try {
			writer = PdfWriter.getInstance(document, fos);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		
		if (writer == null)
			return null;
		
		document.open();
		
		// NOTE: we need to set the output intent for proper viewing. In perticular,
		// Adobe Acrobat Pro X does not set a proper default "Output Preview"
		// (same when rendering a  PDF in Adobe Photoshop) if not specified!
		byte[] iccdata = java.awt.color.ICC_Profile.getInstance(ColorSpace.CS_sRGB).getData();	
		ICC_Profile icc = ICC_Profile.getInstance(iccdata);
		
		try {
			writer.setOutputIntents("Custom", "", "http://www.color.org", "sRGB IEC61966-2.1", icc);
		} catch (IOException e) {
			System.err.println("Could not set ICC profile in PDF output!");
		}

		PdfContentByte cb = writer.getDirectContent();
		Graphics2D g2 = new PdfGraphics2D(cb, width, height);
		
		MathRendering.setRenderOutline(g2, renderTextOutline);
		chart.paint(g2, width, height);
		
		g2.dispose();
		document.close();

		return absPath;
	}

}
