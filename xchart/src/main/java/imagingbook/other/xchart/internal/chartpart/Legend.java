/**
 * Copyright 2015 Knowm Inc. (http://knowm.org) and contributors.
 * Copyright 2011-2015 Xeiam LLC (http://xeiam.com) and contributors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package imagingbook.other.xchart.internal.chartpart;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Shape;
import java.awt.geom.AffineTransform;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.util.LinkedHashMap;
import java.util.Map;

import imagingbook.other.xchart.Series;
import imagingbook.other.xchart.StyleManager;
import imagingbook.other.xchart.StyleManager.ChartType;
import imagingbook.other.xchart.StyleManager.LegendPosition;
import imagingbook.other.xchart.wilbur.TextBox;

/**
 * wilbur: major rewrite
 * 
 * @author timmolter
 */
public class Legend implements ChartPart {

	// TODO: make customizable? (wilbur)
	private static final int LEGEND_MARGIN = 6;
	private static final int BOX_SIZE = 20;
	private static final int MULTI_LINE_SPACE = 3;

	private double legendBoxWidth = 0.0;
	private double legendBoxHeight = 0.0;

	/**
	 * parent
	 */
	private final ChartPainter chartPainter;

	/**
	 * the bounds
	 */
	private Rectangle2D bounds = null; // wilbur

	/**
	 * Constructor
	 *
	 * @param chartPainter
	 */
	public Legend(ChartPainter chartPainter) {
		this.chartPainter = chartPainter;
	}

	/**
	 * determine the width and height of the chart legend
	 */
	public void determineLegendBoxSize() {
		if (!chartPainter.getStyleManager().isLegendVisible()) {
			return;
		}
		StyleManager sm = getChartPainter().getStyleManager();
		boolean isBar = sm.getChartType() == ChartType.Bar;

		// determine legend text content max width
		double legendTextContentMaxWidth = 0;

		// determine legend content height
		double legendContentHeight = 0;

		for (Series series : chartPainter.getAxisPair().getSeriesMap().values()) {
			Map<String, Rectangle2D> seriesBounds = getSeriesTextBounds(series);
			double blockHeight = 0;
			for (Map.Entry<String, Rectangle2D> entry : seriesBounds.entrySet()) {
				blockHeight += entry.getValue().getHeight() + MULTI_LINE_SPACE;
				legendTextContentMaxWidth = Math.max(legendTextContentMaxWidth, entry.getValue().getWidth());
			}
			blockHeight = blockHeight - MULTI_LINE_SPACE;
			blockHeight = Math.max(blockHeight, isBar ? BOX_SIZE : sm.getMarkerSize());
			legendContentHeight += blockHeight + sm.getLegendPadding();
		}
		// determine legend content width
		double legendContentWidth = (isBar) ? BOX_SIZE + sm.getLegendPadding() + legendTextContentMaxWidth
				: sm.getLegendSeriesLineLength() + sm.getLegendPadding() + legendTextContentMaxWidth;
		// Legend Box
		legendBoxWidth = legendContentWidth + 2 * sm.getLegendPadding();
		legendBoxHeight = legendContentHeight + 1 * sm.getLegendPadding();
	}

	/**
	 * wilbur: complete rewrite TODO: CHECK again carefully and clean up
	 * duplicate code!
	 */
	@Override
	public void paint(Graphics2D g) {
		final StyleManager sm = getChartPainter().getStyleManager();
		if (!sm.isLegendVisible()) {
			return;
		}
		Plot plot = chartPainter.getPlot();
		// if the area to draw a chart on is so small, don't even bother
		if (plot.getBounds().getWidth() < 30) {
			return;
		}

		// bounds = new Rectangle2D.Double(); // wilbur: removed
		// g.setFont(chartPainter.getStyleManager().getLegendFont());

		// determine legend's draw position
		Point2D xyOffset = getLegendPosition(sm.getLegendPosition(), plot.getBounds(), sm.getChartPadding());
		final double xOffset = xyOffset.getX();
		final double yOffset = xyOffset.getY();

		// draw the legend box
		drawLegendBox(g, xOffset, yOffset, sm.getLegendBackgroundColor(), sm.getLegendBorderColor());

		// draw series legend content inside legend box
		double startx = xOffset + sm.getLegendPadding();
		double starty = yOffset + sm.getLegendPadding();

		// loop over all series:
		for (Series series : chartPainter.getAxisPair().getSeriesMap().values()) {
			Map<String, Rectangle2D> seriesTextBounds = getSeriesTextBounds(series);

			// find out the height of the text block for this series
			double blockHeight = determineBlockHeight(seriesTextBounds);
			@SuppressWarnings("unused")
			double legendTextContentMaxWidth = determineMaxWidth(seriesTextBounds); // wilbur ever needed??

			// paint the marker for this series
			paintSeriesMarker(series, g, startx, starty, blockHeight); // wilbur: new method below

			// paint the text for this series
			g.setColor(chartPainter.getStyleManager().getChartFontColor());

			switch (sm.getChartType()) {
			case Bar: {
				double multiLineOffset = 0.0;
				final double x = startx + BOX_SIZE + sm.getLegendPadding();

				// draw multiple text lines if necessary
				for (Map.Entry<String, Rectangle2D> entry : seriesTextBounds.entrySet()) {
					double height = entry.getValue().getHeight();
					double centerOffsetY = (Math.max(BOX_SIZE, height) - height) / 2.0;

					TextBox tbox = TextBox.create(entry.getKey(), sm.getLegendFont(), sm.getUseTeXMathFormatting());
					Rectangle2D trect = tbox.getBounds();
					double y = starty - trect.getY() + multiLineOffset + centerOffsetY;
					AffineTransform at = AffineTransform.getTranslateInstance(x, y);

					AffineTransform orig = g.getTransform();
					g.transform(at);
					tbox.paint(g);
					// g.setColor(Color.GREEN.darker()); g.draw(trect);
					g.setTransform(orig);

					multiLineOffset += height + MULTI_LINE_SPACE;
				}
				starty += blockHeight + sm.getLegendPadding();
				break;
			}
			default: {
				double multiLineOffset = 0.0;
				double x = startx + sm.getLegendSeriesLineLength() + sm.getLegendPadding();

				// draw multiple text lines if necessary
				for (Map.Entry<String, Rectangle2D> entry : seriesTextBounds.entrySet()) {
					Rectangle2D lineRect = entry.getValue();
					double height = lineRect.getHeight();
					double centerOffsetY = (Math.max(sm.getMarkerSize(), height) - height) / 2.0;

					TextBox tbox = TextBox.create(entry.getKey(), sm.getLegendFont(), sm.getUseTeXMathFormatting());
					Rectangle2D trect = tbox.getBounds();
					double y = starty - trect.getY() + multiLineOffset + centerOffsetY;
					AffineTransform at = AffineTransform.getTranslateInstance(x, y);

					AffineTransform orig = g.getTransform();
					g.transform(at);
					tbox.paint(g);
					// g.setColor(Color.GREEN.darker()); g.draw(trect);
					g.setTransform(orig);

					multiLineOffset += height + MULTI_LINE_SPACE;
				}
				starty += blockHeight + sm.getLegendPadding();
				break;
			}
			}

		} // end of loop over series

		// bounds
		bounds = new Rectangle2D.Double(xOffset, yOffset, legendBoxWidth, legendBoxHeight);
		// g.setColor(Color.blue);
		// g.draw(bounds);
	}

	private void drawLegendBox(Graphics2D g, double xOffset, double yOffset, Color bgColor, Color borderColor) {
		g.setStroke(new BasicStroke(1, BasicStroke.CAP_BUTT, BasicStroke.JOIN_BEVEL, 10.0f, new float[] { 3.0f, 0.0f },
				0.0f));
		Shape rect = new Rectangle2D.Double(xOffset + 1, yOffset + 1, legendBoxWidth - 2, legendBoxHeight - 2);
		g.setColor(bgColor);
		g.fill(rect);
		g.setColor(borderColor);
		g.draw(rect);
	}

	/**
	 * wilbur: new
	 * 
	 * @param series
	 */
	private void paintSeriesMarker(Series series, Graphics2D g, final double startx, final double starty,
			final double blockHeight) {
		StyleManager sm = getChartPainter().getStyleManager();
		if (sm.getChartType() != ChartType.Bar) {
			// paint line
			if (sm.getChartType() != ChartType.Scatter && series.getStroke() != null) {
				g.setColor(series.getStrokeColor());
				g.setStroke(series.getStroke());
				Shape line = new Line2D.Double(startx, starty + blockHeight / 2.0,
						startx + sm.getLegendSeriesLineLength(), starty + blockHeight / 2.0);
				g.draw(line);
			}
			// paint marker
			if (series.getMarker() != null) {
				g.setColor(series.getMarkerColor());
				series.getMarker().paint(g, startx + sm.getLegendSeriesLineLength() / 2.0, starty + blockHeight / 2.0,
						sm.getMarkerSize());
			}
		} else { // Bar chart
			// paint little box
			if (series.getStroke() != null) {
				g.setColor(series.getStrokeColor());
				Shape rectSmall = new Rectangle2D.Double(startx, starty, BOX_SIZE, BOX_SIZE);
				g.fill(rectSmall);
			}
		}
	}

	/**
	 * wilbur: new method
	 */
	private double determineMaxWidth(Map<String, Rectangle2D> seriesTextBounds) {
		double maxWidth = 0;
		for (Map.Entry<String, Rectangle2D> entry : seriesTextBounds.entrySet()) {
			maxWidth = Math.max(maxWidth, entry.getValue().getWidth());
		}
		return maxWidth;
	}

	/**
	 * wilbur: new method
	 */
	private double determineBlockHeight(Map<String, Rectangle2D> seriesTextBounds) {
		float blockHeight = 0;
		double legendTextContentMaxWidth = 0;

		for (Map.Entry<String, Rectangle2D> entry : seriesTextBounds.entrySet()) {
			blockHeight += entry.getValue().getHeight() + MULTI_LINE_SPACE;
			legendTextContentMaxWidth = Math.max(legendTextContentMaxWidth, entry.getValue().getWidth());
		}

		StyleManager sm = getChartPainter().getStyleManager();
		blockHeight = blockHeight - MULTI_LINE_SPACE;
		blockHeight = Math.max(blockHeight, (sm.getChartType() == ChartType.Bar) ? BOX_SIZE : sm.getMarkerSize());
		return blockHeight;
	}

	/**
	 * wilbur: new method
	 * 
	 * @param pos
	 * @param plotbounds
	 * @param padding
	 * @return
	 */
	private Point2D getLegendPosition(LegendPosition pos, Rectangle2D plotbounds, double padding) {
		double xOffset = 0;
		double yOffset = 0;
		switch (pos) {
		case OutsideE:
			xOffset = chartPainter.getWidth() - legendBoxWidth - padding;
			yOffset = plotbounds.getY() + (plotbounds.getHeight() - legendBoxHeight) / 2.0;
			break;
		case InsideNW:
			xOffset = plotbounds.getX() + LEGEND_MARGIN;
			yOffset = plotbounds.getY() + LEGEND_MARGIN;
			break;
		case InsideNE:
			xOffset = plotbounds.getX() + plotbounds.getWidth() - legendBoxWidth - LEGEND_MARGIN;
			yOffset = plotbounds.getY() + LEGEND_MARGIN;
			break;
		case InsideSE:
			xOffset = plotbounds.getX() + plotbounds.getWidth() - legendBoxWidth - LEGEND_MARGIN;
			yOffset = plotbounds.getY() + plotbounds.getHeight() - legendBoxHeight - LEGEND_MARGIN;
			break;
		case InsideSW:
			xOffset = plotbounds.getX() + LEGEND_MARGIN;
			yOffset = plotbounds.getY() + plotbounds.getHeight() - legendBoxHeight - LEGEND_MARGIN;
			break;
		case InsideN:
			xOffset = plotbounds.getX() + (plotbounds.getWidth() - legendBoxWidth) / 2 + LEGEND_MARGIN;
			yOffset = plotbounds.getY() + LEGEND_MARGIN;
			break;
		}
		return new Point2D.Double(xOffset, yOffset);
	}

	/**
	 * Breaks a multi-line legend tag into a sequence of strings. 
	 * wilbur: major rewrite
	 * 
	 * @param series
	 * @return an insertion-ordered (!) collection of
	 *         <string/bounding-rectangle> pairs.
	 */
	private Map<String, Rectangle2D> getSeriesTextBounds(Series series) {
		String textLines[] = series.getName().split("\\n");
		for (int i = 0; i < textLines.length; i++) {
			textLines[i] = textLines[i].trim();
		}
		Map<String, Rectangle2D> seriesTextBounds = new LinkedHashMap<String, Rectangle2D>(textLines.length);
		StyleManager sm = getChartPainter().getStyleManager();
		for (String line : textLines) {
			// wilbur: modified to use TextBox
			TextBox tbox = TextBox.create(line, getChartPainter().getStyleManager().getLegendFont(), sm.getUseTeXMathFormatting());
			seriesTextBounds.put(line, tbox.getBounds());
		}
		return seriesTextBounds;
	}

	public double getLegendBoxWidth() {
		return legendBoxWidth;
	}

	public double getLegendBoxHeight() {
		return legendBoxHeight;
	}

	@Override
	public Rectangle2D getBounds() {
		return bounds;
	}

	@Override
	public ChartPainter getChartPainter() {
		return chartPainter;
	}

}
