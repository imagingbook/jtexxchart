/**
 * Copyright 2015 Knowm Inc. (http://knowm.org) and contributors.
 * Copyright 2011-2015 Xeiam LLC (http://xeiam.com) and contributors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package imagingbook.other.xchart.internal.chartpart;

import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.geom.Rectangle2D;

import imagingbook.other.xchart.StyleManager;
import imagingbook.other.xchart.wilbur.TextBox;


/**
 * AxisTitle
 */
public class AxisTitle implements ChartPart {

	/** parent */
	private final Axis axis;

	/** the title text */
	private String text = null; // wilbur: was ""

	/** the bounds */
	private Rectangle2D bounds;

	/**
	 * Constructor
	 *
	 * @param axis
	 *            the axis
	 */
	protected AxisTitle(Axis axis) {
		this.axis = axis;
	}

	@Override
	public Rectangle2D getBounds() {
		return bounds;
	}

	// wilbur: added
	public boolean isValidText() {
		return (text != null && !text.trim().isEmpty());
	}

	/**
	 * Check if THIS particular axis is visible 
	 * TODO: wilbur: kludge due to ill class structure
	 * 
	 * @return
	 */
	public boolean isVisible() {
		StyleManager sm = getChartPainter().getStyleManager();
		switch (axis.getDirection()) {
		case X:
			return sm.isXAxisTitleVisible();
		case Y:
			return sm.isYAxisTitleVisible();
		}
		return false;
	}

	/**
	 * wilbur: complete rewrite
	 */
	public void paint(Graphics2D g) {
		final StyleManager sm = getChartPainter().getStyleManager();
		final Axis.Direction direction = axis.getDirection();
		final Rectangle2D paintZone = axis.getPaintZone();

		if (this.isValidText() && this.isVisible()) {
			TextBox tbox = TextBox.create(text, sm.getAxisTitleFont(), sm.getUseTeXMathFormatting());
			Rectangle2D trect = tbox.getBounds(); // non-rotated bounding box
			AffineTransform at = null;

			switch (direction) {
			case X: {
				double x = axis.getPaintZone().getX() + (axis.getPaintZone().getWidth() - trect.getWidth()) / 2.0;
				double y = axis.getPaintZone().getY() + axis.getPaintZone().getHeight() - trect.getHeight();
				at = AffineTransform.getTranslateInstance(x, y - trect.getY());
				bounds = new Rectangle2D.Double(x + trect.getX(), y - sm.getAxisTitlePadding(), trect.getWidth(),
						trect.getHeight() + sm.getAxisTitlePadding());
				break;
			}
			case Y: {
				double x = paintZone.getX() - trect.getY();
				double y = paintZone.getY() + (paintZone.getHeight() + trect.getWidth()) / 2.0;
				at = AffineTransform.getTranslateInstance(x, y);
				at.quadrantRotate(-1);
				bounds = new Rectangle2D.Double(x + trect.getY(), y - trect.getWidth() - trect.getX(),
						trect.getHeight() + sm.getAxisTitlePadding(), trect.getWidth());
				break;
			}
			}

			AffineTransform atOrig = g.getTransform(); // save transform
			g.transform(at);
			g.setColor(sm.getChartFontColor());
			tbox.paint(g);
						
			g.setTransform(atOrig); // restore transform

		} else { // empty or invisible AxisTitle
			switch (direction) {
			case X:
				bounds = new Rectangle2D.Double(axis.getPaintZone().getX(),
						axis.getPaintZone().getY() + axis.getPaintZone().getHeight(), axis.getPaintZone().getWidth(),
						0);
				break;
			case Y:
				bounds = new Rectangle2D.Double(axis.getPaintZone().getX(), axis.getPaintZone().getY(), 0,
						axis.getPaintZone().getHeight()); // Y-axis
				break;
			}
		}
	}

	@Override
	public ChartPainter getChartPainter() {
		return axis.getChartPainter();
	}

	// Getters /////////////////////////////////////////////////

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}
}
