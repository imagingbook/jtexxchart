package imagingbook.other.xchart.wilbur;

import java.awt.Graphics2D;

import org.scilab.forge.jlatexmath.ext.MathRendering;

/**
 * Utility class for connecting to the {@link org.scilab.forge.jlatexmath} 
 * package.
 * 
 * @author wilbur
 *
 */
public abstract class LatexUtil {
	
	/**
	 * Processes the supplied text to check if ti is a 
	 * TeX math formula. If a formula is detected, the substring
	 * enclosed by $...$ is returned. Otherwise {@code null} is
	 * returned.
	 * 
	 * @param text arbitrary input text, must start with '$' to be a legal formula
	 * @return the formula (without surrounding $ signs) or {@code null} if no formula
	 */
	public static String getFormula(String text) {
		String textTrimmed = text.trim();
		int start = textTrimmed.indexOf('$', 0);
		int end   = textTrimmed.lastIndexOf('$');
		//System.out.println("start/end = " + start + "/" + end);
		if (start != 0 || end - start < 2)
			return null;
		if (start < end)
			return textTrimmed.substring(start + 1, end);
		else
			return textTrimmed.substring(start + 1);
	}
	
	
	/**
	 * Convenience method (simply wraps the corresponding jlatexmath method).
	 * Use to hint the associated {@link Graphics2D} instance to
	 * render math texts as either outline shapes or font glyphs
	 * (provided the specific {@link Graphics2D} honors the hint).
	 * 
	 * @param g2 graphics environment
	 * @param outline pass {@code true} to get outline rendering, {@code false} for glyph rendering
	 */
	public static void setRenderOutline(Graphics2D g2, boolean outline) {
		MathRendering.setRenderOutline(g2, outline);
	}
	
	// various tests:
//	public static void main(String[] args) {
//		System.out.println("formula = |" + getFormula("$abc$") + "|");
//		System.out.println("formula = |" + getFormula("$$") + "|");
//		System.out.println("formula = |" + getFormula("   $f(x) = \\sum_{i=0}^{n-1} x_i$") + "|");
//		System.out.println("formula = |" + getFormula("abc$") + "|");
//	}


}
