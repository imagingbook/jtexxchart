package imagingbook.other.xchart.wilbur;

import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Insets;
import java.awt.Shape;
import java.awt.font.FontRenderContext;
import java.awt.font.TextLayout;
import java.awt.geom.AffineTransform;
import java.awt.geom.Rectangle2D;

import org.scilab.forge.jlatexmath.Box;
import org.scilab.forge.jlatexmath.TeXConstants;
import org.scilab.forge.jlatexmath.TeXFormula;
import org.scilab.forge.jlatexmath.TeXIcon;
import org.scilab.forge.jlatexmath.TeXFormula.TeXIconBuilder;

/**
 * Objects of this class can be either a plain text string or a 
 * math formula. Objects of this type can be used in place of
 * ordinary text elements in chart titles, axis labels, legends, etc.
 * Contains nested subclasses {@link TextBox.Math} and {@link TextBox.Math}.
 * 
 * @author wilbur
 *
 */
public abstract class TextBox {
	
	public abstract Rectangle2D getBounds();
	public abstract void paint(Graphics2D g2);
	
	protected final String text;
	protected final Font font;
	
	
	public static TextBox create(String text, Font font, boolean makeTex) {
		//System.out.println("TextBox create : |" + text + "| makeTex = " + makeTex);
		if (makeTex) {
			// test if this could be a TeX formula (enclosed in $...$)
			String formula = LatexUtil.getFormula(text);
			if (formula != null) {
				return new Math(formula, font);
			}
		}
		return new Plain(text, font);
	}
	
	protected TextBox(String text, Font font) {
		this.text = text;
		this.font = font;
	}
	
	// -------------------------------------------------------------
	
	/**
	 * Represents a plain text element.
	 */
	public static class Plain extends TextBox {
		
		private final TextLayout textLayout;

		public Plain(String plaintext, Font font) {
			super(plaintext, font);
			textLayout = new TextLayout(plaintext, font, new FontRenderContext(null, true, false));
		}
		
		@Override
		public Rectangle2D getBounds() {
			return textLayout.getBounds();
		}
		
		@Override
		public void paint(Graphics2D g2) {
			Shape shape = textLayout.getOutline(null);
			g2.fill(shape);
		}
	}

	// -------------------------------------------------------------
	
	/**
	 * Represents a mathematical formula.
	 */
	public static class Math extends TextBox  {
		private final TeXIcon icon;
		
		public Math(String mathtext, Font font) {
			super(mathtext, font);
			TeXFormula formula = new TeXFormula(mathtext);
			TeXIconBuilder tib = formula.new TeXIconBuilder();
			tib.setStyle(TeXConstants.STYLE_DISPLAY);
			tib.setSize(font.getSize2D());
			icon = tib.build();
		}
		
		@Override
		public Rectangle2D getBounds() {
			final double iW = icon.getTrueIconWidth();
			final double iH = icon.getTrueIconHeight();
			final Insets insets = icon.getInsets();
			return new Rectangle2D.Double(insets.left, insets.top, iW, iH);	// better check this!!
		}

		@Override
		public void paint(Graphics2D g2) {
			LatexUtil.setRenderOutline(g2, true);  	// render all glyphs as outline shapes
			//g2.draw(this.getBounds());
			float size = font.getSize2D();
			Box box = icon.getBox();
			Insets insets = icon.getInsets();
			AffineTransform atOrig = g2.getTransform();	// save transform
			// draw the formula box (recursively)
			g2.scale(size, size);
			box.draw(g2, insets.left / size, insets.top / size + box.getHeight());
			g2.setTransform(atOrig);	// restore transform
		}
	}

}
