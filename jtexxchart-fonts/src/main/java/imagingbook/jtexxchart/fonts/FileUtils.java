/*******************************************************************************
 * This software is provided as a supplement to the authors' textbooks on digital
 * image processing published by Springer-Verlag in various languages and editions.
 * Permission to use and distribute this software is granted under the BSD 2-Clause 
 * "Simplified" License (see http://opensource.org/licenses/BSD-2-Clause). 
 * Copyright (c) 2006-2015 Wilhelm Burger, Mark J. Burge. 
 * All rights reserved. Visit http://www.imagingbook.com for additional details.
 *  
 *******************************************************************************/

package imagingbook.jtexxchart.fonts;

import java.awt.Font;
import java.awt.FontFormatException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLClassLoader;
import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Stream;


/**
 * Copied from imagingbook.lib.util.
 * @author WB
 *
 */
public abstract class FileUtils {

    // from: http://www.java2s.com/Tutorial/Java/0180__File/StripFileExtension.htm
    public static String stripFileExtension(String fileName) {
        int dotInd = fileName.lastIndexOf('.');
        // if dot is in the first position,
        // we are dealing with a hidden file rather than an extension
        return (dotInd > 0) ? fileName.substring(0, dotInd) : fileName;
    }
    
    /**
     * Find the path from which a given class was loaded.
     * @param c a class.
     * @return the path of the .class file for the given class or null (e.g.
     * if the class is anonymous).
     */
    public static String getClassPath(Class<?> c) {
    	return c.getClassLoader().getResource(c.getName().replace('.', '/') + ".class").toString();
    }
//	public static String getClassPath(Class<?> c) {
//		String className = c.getSimpleName();
//		URL location = c.getResource(className + ".class");
//		String cpath = null;
//		try {
//			cpath = new File(location.getFile()).getCanonicalPath();
//		} catch (IOException e) {}
//		return cpath;
//	}
    

	
	/**
	 * Find the path to a resource relative to the location of class c.
	 * Example: Assume class C was loaded from file someLocation/C.class
	 * and there is a subfolder someLocation/resources/ that contains 
	 * an image 'lenna.jpg'. Then the absolute path to this image
	 * is obtained by 
	 * String path = getResourcePath(C.class, "resources/lenna.jpg");
	 * 
	 * @param c anchor class 
	 * @param name name of the resource to be found
	 * @return the path for the resource or null if not found.
	 */
	public static String getResourcePath(Class<?> c, String name) {
		URL url = c.getResource(name); 
		//URL url c.getClassLoader().getResource(name); // alternative
		if (url == null) {
			return null;
		}
		else {
			return url.getPath();
		}
	}
	
	/**
	 * Finds a resource relative to the location of class c and returns
	 * a stream to read from.
	 * Example: Assume class C was loaded from file someLocation/C.class
	 * and there is a subfolder someLocation/resources/ that contains 
	 * an image 'lenna.jpg'. Then the absolute path to this image
	 * is obtained by 
	 * 
	 * InputStream strm = getResourceStream(C.class, "resources/lenna.jpg");
	 * 
	 * Use this method to access resources stored in a JAR file!
	 * 
	 * @param c anchor class 
	 * @param name name of the resource to be found
	 * @return a stream for reading the resource or null if not found.
	 */
	public static InputStream getResourceStream(Class<?> c, String name) {
		return c.getResourceAsStream(name);
	}
	
	// ----------------------------------------------------------------
	
	public static void printClassPath() {
		ClassLoader cl = ClassLoader.getSystemClassLoader();
		URL[] urls = ((URLClassLoader) cl).getURLs();
		for (URL url : urls) {
			System.out.println(url.getPath());
		}
	}
	
	
	// ----------------------------------------------------------------
	
	public static Font getFontFromStream(InputStream fontStream) {
		Font f = null;
		try {
			f = Font.createFont(Font.TRUETYPE_FONT, fontStream);
		} catch (FontFormatException e) {
			System.err.println(e.toString());
		} catch (IOException e) {
			System.err.println(e.toString());
		}
		return f;
	}
	
	// ----------------------------------------------------------------

	/**
	 * Use this method to obtain the paths to all files in a directory located
	 * relative to the specified class. This should work in an ordinary file system
	 * as well as a (possibly nested) JAR file.
	 * 
	 * @param theClass class whose source location specifies the root 
	 * @param relPath path relative to the root
	 * @return a sequence of paths or {@code null} if the specified path is not a directory
	 * @throws URISyntaxException
	 * @throws IOException
	 */
	public static Path[] listResources(Class<?> theClass, String relPath) {
		URI uri = null;
		try {
			uri = theClass.getResource(relPath).toURI();
		} catch (URISyntaxException e) {
			System.err.println(e);
			return null;
		}
	    Path resourcePath = null;
	    String scheme = uri.getScheme();
	    if (scheme.equals("file")) {	// ordinary file system
	    	resourcePath = Paths.get(uri);
	    }
	    else if (scheme.equals("jar")) {	// inside a JAR file
	    	FileSystem fs;
			try {
				fs = FileSystems.newFileSystem(uri, Collections.<String, Object>emptyMap());
			} catch (IOException e) {
				System.err.println(e);
				return null;
			}
	        String ssp = uri.getSchemeSpecificPart();
	        int start = ssp.lastIndexOf('!');
	        String inJarPath = ssp.substring(start + 1);  // remove leading part including the last '!'
	        //System.out.println("[listResources] inJarPath = "  + inJarPath);
	        resourcePath = fs.getPath(inJarPath);
	    } 
	    else {
	    	throw new IllegalArgumentException("Cannot handle this path type: " + scheme);
	    }
	    
	    if (!Files.isDirectory(resourcePath)) {
	    	return null; 	// cannot list if no directory
	    }
	    
	    List<Path> rlst = new ArrayList<Path>();
	    // with help from http://stackoverflow.com/questions/1429172/how-do-i-list-the-files-inside-a-jar-file, #10
	    Stream<Path> walk;
		try {
			walk = Files.walk(resourcePath, 1);
		} catch (IOException e) {
			System.err.println(e);
			return null;
		}	    
	    for (Iterator<Path> it = walk.iterator(); it.hasNext();){
	    	Path p = it.next();
//	        try {
//				System.out.println("[listResources] " + p.getFileName().toString() + " " + Files.isRegularFile(p));
//			} catch (Exception e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
	        if (Files.isRegularFile(p) && Files.isReadable(p)) {
	        	rlst.add(p);
	        }
	    }
	    walk.close();
	    return rlst.toArray(new Path[0]);
	}

}
