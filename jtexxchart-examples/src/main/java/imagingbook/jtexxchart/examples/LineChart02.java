/**
 * Copyright 2015 Knowm Inc. (http://knowm.org) and contributors.
 * Copyright 2011-2015 Xeiam LLC (http://xeiam.com) and contributors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package imagingbook.jtexxchart.examples;

import java.awt.Font;
import java.util.ArrayList;
import java.util.List;

import imagingbook.jtexxchart.Settings;
import imagingbook.jtexxchart.fonts.TeXFonts;
import imagingbook.jtexxchart.fonts.TeXFonts.FontFamily;
import imagingbook.other.xchart.Chart;
import imagingbook.other.xchart.Series;
import imagingbook.other.xchart.SeriesColor;
import imagingbook.other.xchart.SeriesLineStyle;
import imagingbook.other.xchart.SeriesMarker;
import imagingbook.other.xchart.SwingWrapper;


/**
 * Sine wave with customized series style
 * <p>
 * * Demonstrates the following:
 * <ul>
 * <li>Customizing the series style properties
 * 
 * Additions by wilbur:
 * JTexXchart feature 1: set legend font (only size is used for math formulas)
 * JTexXchart feature 2: use proper (mathematical) minus signs
 * JTexXchart feature 3: use a TeX (Computer Modern) font to draw the axis labels
 * JTexXchart feature 4: use a TeX formula instead of plain text labels
 */
public class LineChart02 implements ExampleChart {

	public static void main(String[] args) {
		Settings.useSystemLookAndFeel();
		ExampleChart exampleChart = new LineChart02();
		Chart chart = exampleChart.getChart();
		new SwingWrapper(chart).displayChart();
	}


	@Override
	public Chart getChart() {
		// generates sine data
		int size = 30;
		List<Integer> xData = new ArrayList<Integer>();
		List<Double> yData = new ArrayList<Double>();
		for (int i = 0; i <= size; i++) {
			double radians = (Math.PI / (size / 2) * i);
			xData.add(i - size / 2);
			yData.add(-.000001 * Math.sin(radians));
		}

		// Create Chart
		Chart chart = new Chart(800, 600);

		// Customize Chart
		chart.getStyleManager().setChartTitleVisible(false);
		chart.getStyleManager().setLegendVisible(true);		//make legend visible
		
		// *** JTexXchart feature 1: set legend font (only size is used for math formulas)
		chart.getStyleManager().setLegendFont(new Font(Font.SERIF, Font.PLAIN, 18));
		
		// *** JTexXchart feature 2: use proper (mathematical) minus signs
		chart.getStyleManager().setUseMathMinusSign(true);

		// *** JTexXchart feature 3: use a TeX (Computer Modern) font to draw the axis labels
		Font latexfont = TeXFonts.getFont(FontFamily.rm, 14);
		chart.getStyleManager().setAxisTickLabelsFont(latexfont);
		
		
		// Series 1
		// *** JTexXchart feature 4: use a TeX formula instead of plain text labels
		Series series1 = chart.addSeries("$y=\\sin(x)$", xData, yData);
		
		series1.setLineColor(SeriesColor.PURPLE);
		series1.setLineStyle(SeriesLineStyle.DASH_DASH);
		series1.setMarkerColor(SeriesColor.GREEN);
		series1.setMarker(SeriesMarker.SQUARE);

		return chart;
	}

}
