/**
 * Various examples adapted from original XChart demos to demonstrate
 * the additional features of JTexXchart.
 * @author wilbur
 *
 */
package imagingbook.jtexxchart.examples;