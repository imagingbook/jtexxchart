/**
 * Copyright 2015 Knowm Inc. (http://knowm.org) and contributors.
 * Copyright 2011-2015 Xeiam LLC (http://xeiam.com) and contributors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package imagingbook.jtexxchart.examples;

import java.awt.Color;
import java.awt.Font;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import imagingbook.jtexxchart.Settings;
import imagingbook.other.xchart.Chart;
import imagingbook.other.xchart.ChartColor;
import imagingbook.other.xchart.Series;
import imagingbook.other.xchart.SeriesColor;
import imagingbook.other.xchart.SeriesLineStyle;
import imagingbook.other.xchart.SeriesMarker;
import imagingbook.other.xchart.StyleManager.LegendPosition;
import imagingbook.other.xchart.SwingWrapper;

/**
 * Extensive Chart Customization
 */
public class LineChart03 implements ExampleChart {
	
	List<Date> xData = null;
	List<Double> yData = null;

	public static void main(String[] args) {
		Settings.useSystemLookAndFeel();
		ExampleChart exampleChart = new LineChart03();
		Chart chart = exampleChart.getChart();
		new SwingWrapper(chart).displayChart();
	}

	@Override
	public Chart getChart() {
		// Create Chart
		Chart chart = new Chart(800, 600);

		// Customize Chart
		chart.setChartTitle("LineChart03");

		//wilbur: use mathematical symbols
		chart.setXAxisTitle("$X$");
		chart.setYAxisTitle("$Y_1, Y_2$");

		chart.getStyleManager().setPlotBackgroundColor(ChartColor.getAWTColor(ChartColor.GREY));
		chart.getStyleManager().setPlotGridLinesColor(new Color(255, 255, 255));
		chart.getStyleManager().setChartBackgroundColor(Color.WHITE);
		chart.getStyleManager().setLegendBackgroundColor(Color.YELLOW);
		chart.getStyleManager().setChartFontColor(Color.BLACK);

		chart.getStyleManager().setChartTitleBoxBackgroundColor(new Color(0, 255, 0));
		chart.getStyleManager().setChartTitleBoxVisible(true);
		chart.getStyleManager().setChartTitleBoxBorderColor(Color.BLACK);

		chart.getStyleManager().setPlotGridLinesVisible(false);

		chart.getStyleManager().setAxisTickPadding(20);

		chart.getStyleManager().setAxisTickMarkLength(15);

		chart.getStyleManager().setPlotPadding(20);

		chart.getStyleManager().setChartTitleFont(new Font(Font.MONOSPACED, Font.BOLD, 24));
		chart.getStyleManager().setLegendFont(new Font(Font.SERIF, Font.PLAIN, 18));
		chart.getStyleManager().setLegendPosition(LegendPosition.InsideSE);
		chart.getStyleManager().setLegendSeriesLineLength(12);
		chart.getStyleManager().setAxisTitleFont(new Font(Font.SANS_SERIF, Font.ITALIC, 18));
		chart.getStyleManager().setAxisTickLabelsFont(new Font(Font.SERIF, Font.PLAIN, 11));
		chart.getStyleManager().setDatePattern("dd-MMM");
		chart.getStyleManager().setDecimalPattern("#0.000");
		chart.getStyleManager().setLocale(Locale.GERMAN);

		{	// series 1
			makeData();
			Series series = chart.addSeries("$\\text{Series 1: $X \\mapsto Y_1$}$", xData, yData);
			series.setLineColor(SeriesColor.BLUE);
			series.setMarkerColor(Color.ORANGE);
			series.setMarker(SeriesMarker.CIRCLE);
			series.setLineStyle(SeriesLineStyle.SOLID);
		}
		{	// series 2
			makeData();
			Series series = chart.addSeries("$\\text{Series 2: $X \\mapsto Y_2$}$", xData, yData);
			series.setLineColor(SeriesColor.GREEN);
			series.setMarkerColor(Color.RED);
			series.setMarker(SeriesMarker.DIAMOND);
			series.setLineStyle(SeriesLineStyle.DASH_DASH);
		}

		return chart;
	}
	// generates linear data
	private void makeData() {
		xData = new ArrayList<Date>();
		yData = new ArrayList<Double>();

		DateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
		Date date = null;
		for (int i = 1; i <= 10; i++) {

			try {
				date = sdf.parse(i + ".10.2008");
			} catch (ParseException e) {
				e.printStackTrace();
			}
			xData.add(date);
			yData.add(Math.random() * i);
		}
	}
}
