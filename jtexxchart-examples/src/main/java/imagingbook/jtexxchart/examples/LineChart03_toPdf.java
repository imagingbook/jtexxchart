package imagingbook.jtexxchart.examples;


import java.io.FileNotFoundException;
import java.util.Locale;

import imagingbook.jtexxchart.Settings;
import imagingbook.other.xchart.Chart;
import imagingbook.other.xchart.SwingWrapper;
import imagingbook.other.xchart.wilbur_itext.PdfSaverItext;

/**
 * Chart with math content is created and saved to PDF using
 * iText.
 * 
 * @author wilbur
 */
public class LineChart03_toPdf {
	
	static boolean SAVE_PDF = true;
			
	public static void main(String[] args) {
		Locale.setDefault(Locale.US);
		Settings.useSystemLookAndFeel();
		//Box.DEBUG = true;	// draw the TeX boxes
		
		long time = System.currentTimeMillis() / 1000;	//timestamp
		ExampleChart example = new LineChart03();
		Chart chart = example.getChart();
		
		new SwingWrapper(chart).displayChart();
		
		if (SAVE_PDF) {
			String path = "C:\\tmp\\" + example.getClass().getSimpleName() + "-" + time + ".pdf";
			
			try {
				String fullpath = new PdfSaverItext().save(chart, path);
				System.out.println("Saved to " + fullpath);
			} catch (FileNotFoundException e) {
				e.printStackTrace();
				System.out.println("Save failed " + path);
			}
		}
	}

//	public static void useSystemLookAndFeel() {
//		try {
//			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
//			JComponent.setDefaultLocale(Locale.US);
//		} catch (Exception e) {
//		}
//	}
}
