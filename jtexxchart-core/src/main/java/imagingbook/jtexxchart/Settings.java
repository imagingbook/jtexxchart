package imagingbook.jtexxchart;

import java.util.Locale;

import javax.swing.JComponent;
import javax.swing.UIManager;

public class Settings {

	public static boolean useSystemLookAndFeel() {
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
			JComponent.setDefaultLocale(Locale.US);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

}
