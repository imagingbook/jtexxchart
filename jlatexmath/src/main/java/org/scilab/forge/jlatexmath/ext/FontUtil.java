package org.scilab.forge.jlatexmath.ext;

import java.awt.Font;

import org.scilab.forge.jlatexmath.DefaultTeXFontParser;

/**
 * Something like this would be nice to have...
 * 
 * @author wilbur
 *
 */
public abstract class FontUtil {
	
	public enum FontVersion {
		rm, ti, bx, bxti, ss, ssi, tt;
	}
	
	public static Font getLatexFont(FontVersion version, double size) {
		String fontpath = null;
		switch (version) {
		case bx:
			fontpath = "fonts/latin/optional/jlm_cmbx10.ttf";
			break;
		case bxti:
			fontpath = "fonts/latin/optional/jlm_cmbxti10.ttf";
			break;
		case ti:
			fontpath = "fonts/latin/optional/jlm_cmti10.ttf";
			break;
		case rm:
			fontpath = "fonts/latin/jlm_cmr10.ttf";
			break;
		case ssi:
			fontpath = "fonts/latin/optional/jlm_cmssi10.ttf";
			break;
		case ss:
			fontpath = "fonts/latin/optional/jlm_cmss10.ttf";
			break;
		case tt:
			fontpath = "fonts/latin/optional/jlm_cmtt10.ttf";
			break;
		}

		Font f = DefaultTeXFontParser.createFont(fontpath);
		return f.deriveFont((float)size);
	}

}
